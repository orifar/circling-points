PolarPoint[] p;
int POINTS;
float last_degree = 0;

void setup()
{

  fullScreen();
  POINTS = displayHeight/20;
  createPoints();
}

void draw()
{
  background(255, 255, 255);
  translate(width/2, height/2);
  if(last_degree < 4*PI && p[1].m_degrees > 4*PI)
  {
     createPoints();
  }
  
  last_degree = p[1].m_degrees;
  for(int i=0; i<POINTS; i++)
  {
    p[i].update();
  }
  
  for(int i=0; i<POINTS-1; i++)
  {
    fill(i,i,i);
    line(p[i].m_x, p[i].m_y,p[i+1].m_x,p[i+1].m_y);
  }
}

void createPoints()
{
  p = new PolarPoint[POINTS];
  for(int i=0; i<POINTS; i++)
  {
    p[i] = new PolarPoint(i*10, i+1);
  }
}