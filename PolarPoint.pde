class PolarPoint
{
  public float m_x;
  public float m_y;
  float m_degrees;
  int m_dist;
  float m_rate;
  public PolarPoint(int dist, float rotations)
  {
    this.m_dist = dist;
    this.m_rate = (rotations)/10000;
    this.m_x = dist;
    this.m_y = 0;
    this.m_degrees = 0;
  }
  public void update()
  {
    this.m_degrees+=m_rate;
    this.m_x = (m_dist*cos(m_degrees));
    this.m_y = (m_dist*sin(m_degrees));
    fill(m_dist,abs(255-m_dist),m_dist);
    ellipse(m_x,m_y, 5, 5);
  }
  
}
